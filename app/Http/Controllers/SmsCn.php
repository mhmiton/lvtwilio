<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Twilio\Rest\Client;

class SmsCn extends Controller
{
    public function index()
    {
        return view('sms');
    }

    public function smsSend(Request $request)
    {
        $sid            = 'AC8fd8345dc1a9c7162390a2c69862da5c';
        $token          = '87a6da178078b7f2b84205342fa72404';
        $twilioClient   = new Client($sid, $token);

        $phone          = $request->phone;
        $message        = $request->message;
        $from           = '+12014821764';

        try {
            $send = $twilioClient->messages->create($phone, [
                'from'    => $from,
                'body'    => $message
            ]);
            
            dd($send);
        } catch (\Exception $ex) {
            dd($ex);
            session()->flash('msg', ['type' => 'error', 'text' => 'Some error occur, sorry for inconvenient']);
            return redirect()->route('/');
        }

    }
}
