<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Send SMS With Twilio</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/notify.min.js"></script>
</head>

<body>
    <div class="container">
        <div class="row justify-content-center" style="margin-top: 150px;">
            <div class="col-md-6">
                <div class="card">
                    <h5 class="card-header">Send SMS With Twilio</h5>
                    <div class="card-body">
                        <form action="{{ route('sms.send') }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label>Phone</label>
                                <input class="form-control" type="text" name="phone" min="0" required>
                            </div>

                            <div class="form-group">
                                <label>Message</label>
                                <textarea class="form-control" name="message" required></textarea>
                            </div>

                            <div>
                                <button type="submit" class="btn btn-success">Send Now</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

    @php
        $msg  = session()->get('msg');
        $text = $msg['text'] ?? '';
        $type = $msg['type'] ?? '';
    @endphp

    @if($text & $type)
        <script type="text/javascript">
            {{-- $.notify('{{$text}}','{{$type}}'); --}}
            swal({title:'{{$text}}', icon:'{{$type}}', button:true});
        </script>
    @endif
</body>

</html>